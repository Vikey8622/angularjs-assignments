assignmentApp.controller('assignmentController', [ '$scope', function($scope) {
    "use strict";

    $scope.emailSubmitted = false;

    $scope.emailSubmit = () => {
        $scope.emailSubmitted = true;
        $scope.emailForm.$setPristine();
    }

    $scope.size = $scope.rangeValue = 20;
    
    /* 
        Assignment 3: 
        Create an async function (setTimeout with console log).
        Call this function multiple times using chained promises and explain the behaviour.
        The program should contain the following
        1. Normal script
        2. setTimout
        3. Console log inside a Promise resolve 
    */

    const displayAsync = (data) => {
        let q = $q.defer();

        setTimeout(() => {
            console.log(data);
            q.resolve(data);
        }, 1000)

        return q.promise;
    }

    displayAsync(100)
        .then((res) => displayAsync(res + 100))    
        .then((res) => displayAsync(res + 100))

        /*
            Assignment 4:
            var add = function (a, b) {
            return a + b;
            };
            add(1, 3); //returns 4
            var addThree = add.bind(null, 3);  //this = null.  a = 3
            addThree(4);    
    
            But how can I bind the 2nd argument and leave the first as is. In other words how can I bind to 'b' only? 
        */
    
            var add = (a, b) => a + b;
         
            // Writing own bind method
    
             var bindOnlySec = (addFn, ...argsToBind) => {
                 return (...args) => {
                     return addFn(...argsToBind, ...args); /* Here args-> a
                                                                   argsToBind -> b */
                 };
             }
             
             var addThree = bindOnlySec(add, 3); // b -> 3
             console.log(addThree(1)); //a -> 1
}]);
assignmentApp.directive('validateEmail', () => {
    const EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

    const emailValidator = (scope, element, attr, ctrl) => {

        ctrl.$validators.emailInvalid = (modelValue, viewValue) => {                    
            if(ctrl.$isEmpty(modelValue)){
                scope.emailSubmitted = false;
            }
            return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(viewValue);
        }

        ctrl.$validators.valueEmpty = (modelValue, viewValue) => {
            return ctrl.$isEmpty(modelValue)
        }

        element.on("keyup", () => {
            const isMatchRegex = EMAIL_REGEXP.test(element.val());
            if( isMatchRegex && element.hasClass('invalid-email') || element.val() == ''){
                element.removeClass('invalid-email');
            }else if(isMatchRegex == false && !element.hasClass('invalid-email')){
                element.addClass('invalid-email');
            }
        });

    }

    return {
        require: 'ngModel',
        scope: {
            emailSubmitted: '='
        },
        link: emailValidator
    };
})
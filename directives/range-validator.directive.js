assignmentApp.
    directive('validateRange', () => {

        const rangeValidator = (scope, element, attrs, ngModelCtrl) => {

            if(attrs.min){
            
                const maxValidator = (value) => {
                    return value > attrs.max ?  attrs.max : value;
                }

                const minValidator = (value) => {
                    return value <= attrs.min ? attrs.min : value;
                }

                const parseViewValue = (value) => {
                    value = parseInt(value);
                    let viewValue;

                    viewValue = maxValidator(value);
                    viewValue = minValidator(viewValue);

                    ngModelCtrl.$setViewValue(viewValue);
                    ngModelCtrl.$render();

                    return viewValue;
                };

                const formatModelValue = (modelValue) => {
                    ngModelCtrl.$modelValue = modelValue;
                    return modelValue;
                }
                ngModelCtrl.$parsers.push(parseViewValue);
                ngModelCtrl.$formatters.push(formatModelValue);
            }
            

            const removeSpecialChars = (value) => {
                if(!value){
                    return '';
                }

                let valueToString = parseInt(value, 10).toString();                
                let num = valueToString.replace(/[^\d\.]*/g, '');

                ngModelCtrl.$setViewValue(num);
                ngModelCtrl.$render();

                return num;
            }


            ngModelCtrl.$parsers.push(removeSpecialChars);

            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                if(newValue > 100 || newValue < 0){                   
                    ngModelCtrl.$setViewValue(oldValue);
                    ngModelCtrl.$render();
                }
            });

        };

        return {
            require: 'ngModel',
            restrict: 'A',
            link: rangeValidator
        }
    });